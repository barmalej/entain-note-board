# ui

This library was generated with [Nx](https://nx.dev).

## UI library

This library contains all UI components. Components built with power of [React JS](https://reactjs.org/) and [Fels JS](https://fela.js.org/)

## Running unit tests

Run `nx test ui` to execute the unit tests via [Jest](https://jestjs.io).

## Running storybook

Run `nx storybook ui` to view the storybook for Entain Note Board app
