import { testRenderer as render } from '@entain/fela'

import WhiteBoard from './white-board'

describe('WhiteBoard', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <WhiteBoard notes={{}} user={{ id: '123', name: 'test' }} />,
    )
    expect(baseElement).toBeTruthy()
  })
})
