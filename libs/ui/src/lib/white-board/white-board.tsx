import { getDatabase, updateNote } from '@entain/db'
import { getRandomColor, IUser, Notes } from '@entain/utils'
import { push, ref, remove, set, update } from 'firebase/database'
import { DragEvent, MouseEvent } from 'react'
import { Box } from '../generic/box/box'
import Note from '../note/note'
import * as styles from './white-board.style'

export interface WhiteBoardProps {
  notes: Notes
  user: IUser
}

const onDragOver = (e: DragEvent) => {
  e.preventDefault()
  e.stopPropagation()
}

const getNoteData = (
  e: DragEvent<HTMLDivElement>,
): [string, number, number] => {
  const noteId = e.dataTransfer.getData('target')
  const [x, y] = e.dataTransfer
    .getData('coords')
    .split(',')
    .map((v) => parseInt(v, 10))
  e.dataTransfer.clearData()

  return [noteId, x, y]
}

export const WhiteBoard = ({ notes, user }: WhiteBoardProps) => {
  const db = getDatabase()

  const onDrop = (e: DragEvent<HTMLDivElement>) => {
    const [noteId, x, y] = getNoteData(e)
    const state = { ...notes }
    const note = { ...state[noteId] }
    const left = e.clientX + x
    const top = e.clientY + y

    note.left = left
    note.top = top
    state[noteId] = note

    update(ref(db), {
      [`notes/${noteId}/left`]: left,
      [`notes/${noteId}/top`]: top,
    })

    e.preventDefault()
    e.stopPropagation()
  }

  const onClick = (e: MouseEvent) => {
    e.preventDefault()

    if (user?.id && user?.name) {
      const notesRef = ref(db, 'notes')
      const newNoteRef = push(notesRef)
      set(newNoteRef, {
        id: newNoteRef.key,
        title: user.name,
        left: e.clientX,
        top: e.clientY,
        color: getRandomColor(),
        uid: user.id,
      })
    }
  }

  const removeNote = (id: string) => () => {
    remove(ref(getDatabase(), `notes/${id}`))
  }

  const onInputEnd = (id: string) => (titleText: string) => {
    updateNote(id, 'text', titleText || '')
  }

  return (
    <Box
      style={styles.whiteBoard}
      onDrop={onDrop}
      onDragOver={onDragOver}
      onClick={onClick}
    >
      {Object.values(notes || {}).map((note) => (
        <Note
          key={`note-${note.id}`}
          note={note}
          user={user}
          remove={removeNote(note.id)}
          onInputEnd={onInputEnd(note.id)}
        />
      ))}
    </Box>
  )
}

export default WhiteBoard
