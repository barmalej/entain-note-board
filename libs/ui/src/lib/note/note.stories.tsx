import { FelaProvider, getFelaRenderer } from '@entain/fela'
import { colors } from '@entain/utils'
import { Story, Meta } from '@storybook/react'
import Box from '../generic/box/box'
import { Note, NoteProps } from './note'

export default {
  component: Note,
  title: 'Note',
} as Meta

const Template: Story<NoteProps> = (args) => (
  <FelaProvider renderer={getFelaRenderer()}>
    <Box
      style={{
        width: '100vw',
        height: '100vh',
        background: colors.white,
      }}
    >
      <Note {...args} />
    </Box>
  </FelaProvider>
)

export const Primary = Template.bind({})
Primary.args = {
  note: {
    id: 'note-1',
    title: 'Test user 1',
    text: 'This test note belongs to user',
    top: 100,
    left: 100,
    color: 'frenchPink',
    uid: '123',
  },
  user: { id: '123', name: 'Test user 1' },
}

export const Secondary = Template.bind({})
Secondary.args = {
  note: {
    id: 'note-2',
    title: 'Test user 2',
    text: 'This test note does not belong to user',
    top: 100,
    left: 100,
    color: 'electricBlue',
    uid: '123',
  },
  user: { id: '321', name: 'Test user 2' },
}
