import { INote, IUser } from '@entain/utils'
import { DragEvent, MouseEvent } from 'react'
import { Box } from '../generic/box/box'
import { Heading } from './heading'
import * as styles from './note.style'
import { Text } from './text'

export interface NoteProps {
  note: INote
  user: IUser
  remove?: () => void
  onInputEnd?: (title: string) => void
}

const getCoords = (event: DragEvent<HTMLDivElement>) => {
  const style = window.getComputedStyle(event.currentTarget, null)

  function getProp(prop: 'left' | 'top') {
    return parseInt(style.getPropertyValue(prop), 10)
  }

  return `${getProp('left') - event.clientX},${getProp('top') - event.clientY}`
}

const onDragStart = (e: DragEvent<HTMLDivElement>) => {
  const coords = getCoords(e)
  e.dataTransfer.setData('coords', coords)
  e.dataTransfer.setData('target', e.currentTarget.id)
}

const onClick = (e: MouseEvent) => {
  e.stopPropagation()
}

export const Note = ({
  note: { id, top, left, color, title, text, uid },
  user,
  remove,
  onInputEnd,
}: NoteProps) => {
  const belongToUser = user.id === uid

  return (
    <Box
      id={id}
      style={styles.note({ top, left, color, editable: belongToUser })}
      onDragStart={onDragStart}
      onClick={onClick}
      draggable={belongToUser}
    >
      {!belongToUser && <Heading noteId={id} title={title} color={color} />}
      <Text
        noteId={id}
        text={text}
        editable={belongToUser}
        onInputEnd={onInputEnd}
      />
      {belongToUser && (
        <Box style={styles.close} onClick={remove}>
          &#10007;
        </Box>
      )}
    </Box>
  )
}

export default Note
