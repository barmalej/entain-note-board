import { testRenderer as render } from '@entain/fela'

import Note from './note'

describe('Note', () => {
  it('should have no heading for owner', () => {
    const { baseElement } = render(
      <Note
        note={{
          id: 'note-1',
          title: 'Note 1',
          text: 'This is note one',
          color: 'electricBlue',
          uid: '123',
        }}
        user={{ id: '123', name: 'test' }}
      />,
    )

    expect(baseElement).toBeTruthy()
    expect(baseElement.querySelector('h3')).toBeFalsy()
    expect(baseElement.querySelectorAll('div').length).toBe(4)
  })

  it('should not be removable for guest', () => {
    const { baseElement } = render(
      <Note
        note={{
          id: 'note-1',
          title: 'Note 1',
          text: 'This is note one',
          color: 'electricBlue',
          uid: '123',
        }}
        user={{ id: '321', name: 'test' }}
      />,
    )

    expect(baseElement).toBeTruthy()
    expect(baseElement.querySelector('h3')).toBeTruthy()
    expect(baseElement.querySelectorAll('div').length).toBe(3)
  })
})
