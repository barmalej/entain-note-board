import { NoteColor } from '@entain/utils'
import { Box } from '../generic/box/box'
import * as styles from './note.style'

interface HeadingProps {
  title: string
  noteId: string
  color: NoteColor
}

export const Heading = ({ title, color }: HeadingProps) => (
  <Box as="h3" style={styles.heading}>
    <Box as="span" style={styles.createdBy(color)}>
      created by
    </Box>
    &nbsp;{title}
  </Box>
)

export default Heading
