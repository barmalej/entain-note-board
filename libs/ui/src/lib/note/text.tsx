import { updateNote } from '@entain/db'
import { ChangeEvent, useEffect, useRef, useState } from 'react'
import { Box } from '../generic/box/box'
import * as styles from './note.style'

interface TextProps {
  text: string
  noteId: string
  editable: boolean
  onInputEnd?: (title: string) => void
}

export const Text = ({ text, editable, onInputEnd }: TextProps) => {
  const [titleText, setTitleText] = useState(text)
  const ref = useRef(text)
  useEffect(() => {
    // debounce update until user stops typing
    const timeout = setTimeout(() => {
      if (typeof onInputEnd === 'function') {
        onInputEnd(titleText)
      }
    }, 300)

    return () => clearTimeout(timeout)
  }, [titleText])

  const onInput = (e: ChangeEvent<HTMLHeadingElement>) => {
    const value = e.target.innerText

    setTitleText(value)
  }

  const nonEditableProps = {
    onInput,
    style: styles.text(editable),
    contentEditable: editable,
    suppressContentEditableWarning: editable,
  }

  return editable ? (
    // This is a workaround to prevent cursor jumping to the beginning of text on every render
    <Box
      {...nonEditableProps}
      dangerouslySetInnerHTML={{ __html: ref.current }}
    />
  ) : (
    <Box {...nonEditableProps}>{text}</Box>
  )
}

export default Text
