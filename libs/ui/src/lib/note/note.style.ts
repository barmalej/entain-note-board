import { colors, NoteColor, notesColors } from '@entain/utils'
import { IStyle } from 'fela'

type NotesStyle = (args: {
  top?: number
  left?: number
  color?: NoteColor
  editable?: boolean
}) => IStyle

export const note: NotesStyle = ({
  top = 0,
  left = 0,
  color = 'canary',
  editable = false,
}) => ({
  fontFamily: 'Indie Flower, cursive',
  fontWeight: 300,
  position: 'absolute',
  top,
  left,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  backgroundColor: notesColors[color],
  width: '225px',
  height: '255px',
  padding: '20px',
  cursor: editable ? 'move' : 'default',
  boxShadow: '5px 5px 7px rgba(33,33,33,.7)',
  transition: 'transform .15s linear',
  transform: 'rotate(-6deg)',
  ':nth-child(even)': {
    transform: 'rotate(4deg)',
  },
  ':nth-child(3n)': {
    transform: 'rotate(-3deg)',
  },
  ':nth-child(5n)': {
    transform: 'rotate(5deg)',
  },
  onHover: {
    boxShadow: '10px 10px 7px rgba(0,0,0,.7)',
    transform: 'scale(1.25)',
    zIndex: 10,
  },
})

export const close: IStyle = {
  position: 'absolute',
  top: '10px',
  right: '10px',
  cursor: 'pointer',
}

export const text: (editable: boolean) => IStyle = (editable) => ({
  onHover: {
    cursor: editable ? 'pointer' : 'default',
  },
  onFocus: {
    cursor: 'text',
  },
  overflowWrap: 'break-word',
  wordBreak: 'break-word',
  hyphens: 'auto',
  width: '100%',
  height: '100%',
  overflowX: 'hidden',
  overflowY: 'auto',
})

export const heading: IStyle = {
  textAlign: 'center',
  overflowWrap: 'break-word',
  wordBreak: 'break-word',
  hyphens: 'auto',
}

const colorGray = ['canary', 'maximumYellow', 'electricBlue']

export const createdBy: (color: NoteColor) => IStyle = (color) => ({
  fontStyle: 'italic',
  fontSize: '14px',
  fontFamily: 'Helvetica, snas-serif',
  fontWeight: 300,
  color: colorGray.includes(color) ? colors.darkGray : colors.white,
})
