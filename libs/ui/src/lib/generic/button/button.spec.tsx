import { testRenderer as render } from '@entain/fela'

import Button from './button'

describe('Button', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Button />)
    expect(baseElement).toBeTruthy()
  })
})
