import { colors } from '@entain/utils'
import { IStyle } from 'fela'

export const button: IStyle = {
  padding: '10px 20px',
  background: colors.brandiesBlue,
  color: colors.white,
  textDecoration: 'none',
  cursor: 'pointer',
  lineHeight: 1,
  fontSize: '16px',
  transition: 'background 250ms ease-in-out, transform 150ms ease',

  onActive: {
    transform: 'scale(0.98)',
  },
  onHover: {
    background: colors.absoluteZero,
  },
  onFocus: {
    background: colors.absoluteZero,
    outline: `1px solid ${colors.white}`,
    outlineOffset: '-2px',
  },
  onDisabled: {
    filter: 'brightness(0.85)',
    color: colors.brightGray,
  },
}
