import { EventHandler, FC, MouseEvent } from 'react'
import { Box } from '../box/box'
import * as styles from './button.style'

export interface ButtonProps {
  disabled?: boolean
  onClick?: EventHandler<MouseEvent>
}

export const Button: FC<ButtonProps> = ({
  children,
  onClick,
  disabled = false,
}) => (
  <Box as="button" style={styles.button} disabled={disabled} onClick={onClick}>
    {children}
  </Box>
)

export default Button
