import { FelaProvider, getFelaRenderer } from '@entain/fela'
import { Story, Meta } from '@storybook/react'
import { Box } from '../box/box'
import { Modal, ModalProps } from './modal'

export default {
  component: Modal,
  title: 'Modal',
} as Meta

const Template: Story<ModalProps> = (args) => (
  <FelaProvider renderer={getFelaRenderer()}>
    <Modal show={true} style={{ width: '300px' }}>
      <Box
        style={{
          padding: '20px',
        }}
      >
        Some modal content
      </Box>
    </Modal>
  </FelaProvider>
)

export const Primary = Template.bind({})
Primary.args = {}
