import { colors } from '@entain/utils'
import { IStyle } from 'fela'

type ModalStyle = (show: boolean) => IStyle

export const modal: ModalStyle = (show) => ({
  display: show ? 'block' : 'none',
  zIndex: 1010,
  position: 'fixed',
  backgroundColor: colors.white,
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
})

export const overlay: ModalStyle = (show) => ({
  display: show ? 'block' : 'none',
  zIndex: 1000,
  backgroundColor: 'rgba(0,0,0,0.5)',
  position: 'fixed',
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
})
