import { testRenderer as render } from '@entain/fela'

import Modal from './modal'

describe('Modal', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Modal show />)
    expect(baseElement).toBeTruthy()
  })
})
