import { IStyle } from 'fela'
import { FC } from 'react'
import { Box } from '../box/box'
import * as styles from './modal.style'

export interface ModalProps {
  show: boolean
  style?: IStyle
}

export const Modal: FC<ModalProps> = ({ children, style, show }) => {
  return (
    <>
      <Box style={{ ...styles.modal(show), ...style }}>{children}</Box>
      <Box style={styles.overlay(show)}></Box>
    </>
  )
}

export default Modal
