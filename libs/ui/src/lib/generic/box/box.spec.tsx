import { testRenderer as render } from '@entain/fela'

import { Box } from './box'

describe('Box', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Box />)
    expect(baseElement).toBeTruthy()
  })

  it('should render as h1', () => {
    const { baseElement } = render(<Box as="h1" />)
    expect(baseElement.querySelector('h1')?.tagName).toBe('H1')
  })
})
