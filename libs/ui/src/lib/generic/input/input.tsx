import { ChangeEvent, KeyboardEvent, EventHandler } from 'react'
import { Box } from '../box/box'
import * as styles from './input.style'

export interface InputProps {
  placeholder?: string
  onInput?: EventHandler<ChangeEvent<HTMLInputElement>>
  onKeyDown?: EventHandler<KeyboardEvent<HTMLInputElement>>
}

export const Input = ({ placeholder, onInput, onKeyDown }: InputProps) => (
  <Box
    as="input"
    placeholder={placeholder}
    style={styles.input}
    onInput={onInput}
    onKeyDown={onKeyDown}
  />
)

export default Input
