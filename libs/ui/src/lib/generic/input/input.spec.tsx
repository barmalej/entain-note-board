import { testRenderer as render } from '@entain/fela'

import Input from './input'

describe('Input', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Input />)
    expect(baseElement).toBeTruthy()
  })
})
