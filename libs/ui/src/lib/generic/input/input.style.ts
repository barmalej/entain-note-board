import { colors } from '@entain/utils'
import { IStyle } from 'fela'

export const input: IStyle = {
  padding: '5px 10px',
  backgroundColor: colors.white,
  width: '100%',
  onFocus: {
    outline: '1px solid #aaa',
  },
}
