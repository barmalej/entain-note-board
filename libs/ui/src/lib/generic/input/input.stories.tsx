import { FelaProvider, getFelaRenderer } from '@entain/fela'
import { Story, Meta } from '@storybook/react'
import Box from '../box/box'
import { Input, InputProps } from './input'

export default {
  component: Input,
  title: 'Input',
} as Meta

const Template: Story<InputProps> = (args) => (
  <FelaProvider renderer={getFelaRenderer()}>
    <Box style={{ width: '200px' }}>
      <Input {...args} />
    </Box>
  </FelaProvider>
)

export const Primary = Template.bind({})
Primary.args = { placeholder: 'Write something' }
