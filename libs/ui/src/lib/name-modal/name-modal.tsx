import { getDatabase } from '@entain/db'
import { getAuth } from 'firebase/auth'
import { ref, set } from 'firebase/database'
import { ChangeEvent, KeyboardEvent, useState } from 'react'
import { Box } from '../generic/box/box'
import Button from '../generic/button/button'
import Input from '../generic/input/input'
import Modal from '../generic/modal/modal'
import * as styles from './name-modal.style'

export interface NameModalProps {
  show: boolean
}

export const NameModal = ({ show }: NameModalProps) => {
  const [name, setName] = useState('')

  const onInput = (e: ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value)
  }

  const onClick = async () => {
    const auth = getAuth()
    const id = auth.currentUser?.uid

    if (id) {
      const usr = { id, name }
      await set(ref(getDatabase(), `users/${id}`), usr)
    }
  }

  const onKeyDown = (e: KeyboardEvent) => {
    if (e.key === 'Enter') {
      onClick()
    }
  }

  return (
    <Modal show={show}>
      <Box style={styles.wrapper}>
        <Box as="h3" style={styles.heading}>
          Please enter your name!
        </Box>
        <Box style={styles.inputWrapper}>
          <Input
            placeholder="e.g. John"
            onInput={onInput}
            onKeyDown={onKeyDown}
          />
        </Box>
        <Box style={styles.buttonWrapper}>
          <Button onClick={onClick} disabled={!name}>
            SAVE
          </Button>
        </Box>
      </Box>
    </Modal>
  )
}

export default NameModal
