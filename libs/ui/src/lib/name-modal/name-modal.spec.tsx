import { testRenderer as render } from '@entain/fela'

import NameModal from './name-modal'

describe('NameModal', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<NameModal show />)
    expect(baseElement).toBeTruthy()
  })
})
