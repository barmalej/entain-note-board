import { IStyle } from 'fela'

export const wrapper: IStyle = {
  padding: '20px',
}

export const heading: IStyle = { textAlign: 'center', fontSize: '28px' }

export const inputWrapper: IStyle = {
  display: 'flex',
  paddingTop: '30px',
  width: '100%',
}

export const buttonWrapper: IStyle = {
  display: 'flex',
  justifyContent: 'flex-end',
  paddingTop: '15px',
}
