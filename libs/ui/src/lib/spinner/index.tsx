import dynamic from 'next/dynamic'

export const Spinner = dynamic(() => import('./spinner'), { ssr: false })
