import { FelaProvider, getFelaRenderer } from '@entain/fela'
import { getRandomColor, notesColors } from '@entain/utils'
import { Story, Meta } from '@storybook/react'
import { Spinner, SpinnerProps } from './spinner'

export default {
  component: Spinner,
  title: 'Spinner',
} as Meta

const Template: Story<SpinnerProps> = (args) => (
  <FelaProvider renderer={getFelaRenderer()}>
    <Spinner {...args} />
  </FelaProvider>
)

export const Primary = Template.bind({})
Primary.args = { color: notesColors[getRandomColor()] }
