import { testRenderer as render } from '@entain/fela'

import Spinner from './spinner'

describe('Spinner', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Spinner color="#FFF" />)
    expect(baseElement).toBeTruthy()
  })
})
