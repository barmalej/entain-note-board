import { getRandomColor, notesColors } from '@entain/utils'
import { IStyle } from 'fela'
import { useContext } from 'react'
import { RendererContext } from 'react-fela'
import { Box } from '../generic/box/box'

interface KeyframeState {
  [k: string]: {
    transform: string
  }
}

const keyFrame = (state: KeyframeState) => ({
  '0%, 20%, 80%, 100%': {
    transform: state.scale1.transform,
  },
  '50%': {
    transform: state.scale1_5.transform,
  },
})

const spinnerStyle: (animationName: string, color: string) => IStyle = (
  animationName,
  color,
) => ({
  display: 'inline-block',
  position: 'relative',
  width: '80px',
  height: '80px',

  '> div': {
    position: 'absolute',
    width: '6px',
    height: '6px',
    background: color,
    borderRadius: '50%',
    animation: `${animationName} 1.2s linear infinite`,
  },
  '> :nth-child(1)': {
    animationDelay: '0s',
    top: '37px',
    left: '66px',
  },
  '> :nth-child(2)': {
    animationDelay: '-0.1s',
    top: '22px',
    left: '62px',
  },
  '> :nth-child(3)': {
    animationDelay: '-0.2s',
    top: '11px',
    left: '52px',
  },
  '> :nth-child(4)': {
    animationDelay: '-0.3s',
    top: '7px',
    left: '37px',
  },
  '> :nth-child(5)': {
    animationDelay: '-0.4s',
    top: '11px',
    left: '22px',
  },
  '> :nth-child(6)': {
    animationDelay: '-0.5s',
    top: '22px',
    left: '11px',
  },
  '> :nth-child(7)': {
    animationDelay: '-0.6s',
    top: '37px',
    left: '7px',
  },
  '> :nth-child(8)': {
    animationDelay: '-0.7s',
    top: '52px',
    left: '11px',
  },
  '> :nth-child(9)': {
    animationDelay: '-0.8s',
    top: '62px',
    left: '22px',
  },
  '> :nth-child(10)': {
    animationDelay: '-0.9s',
    top: '66px',
    left: '37px',
  },
  '> :nth-child(11)': {
    animationDelay: '-1s',
    top: '62px',
    left: '52px',
  },
  '> :nth-child(12)': {
    animationDelay: '-1.1s',
    top: '52px',
    left: '62px',
  },
})

export interface SpinnerProps {
  color: string
}

export const Spinner = ({ color }: SpinnerProps) => {
  const renderer = useContext(RendererContext)
  const _animationName = renderer.renderKeyframe(keyFrame, {
    scale1: { transform: 'scale(1)' },
    scale1_5: { transform: 'scale(1.5)' },
  })

  return (
    <Box style={spinnerStyle(_animationName, color)}>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </Box>
  )
}

export default Spinner
