import namedKeys from 'fela-plugin-named-keys'

const namedKeysPlugin = namedKeys({
  landscape: '@media (orientation: landscape)',
  portrait: '@media (orientation: portrait)',
})

export default namedKeysPlugin
