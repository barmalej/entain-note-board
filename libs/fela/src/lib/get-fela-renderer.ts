import { createRenderer } from 'fela'
import webPreset from 'fela-preset-web'
import pseudoClass from 'fela-plugin-friendly-pseudo-class'
import namedKeysPlugin from './fela-named-keys'
import { globalDefaultStyle } from '@entain/utils'

export function getFelaRenderer() {
  const renderer = createRenderer({
    plugins: [...webPreset, pseudoClass(), namedKeysPlugin],
  })
  renderer.renderStatic(globalDefaultStyle)

  return renderer
}
