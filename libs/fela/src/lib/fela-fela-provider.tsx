import { IRenderer } from 'fela'
import { Component } from 'react'
import { RendererProvider } from 'react-fela'
import { getFelaRenderer } from './get-fela-renderer'

const fallbackRenderer = getFelaRenderer()

export class FelaProvider extends Component<{ renderer: IRenderer }> {
  render() {
    const renderer = this.props.renderer || fallbackRenderer
    return (
      <RendererProvider renderer={renderer}>
        {this.props.children}
      </RendererProvider>
    )
  }
}
