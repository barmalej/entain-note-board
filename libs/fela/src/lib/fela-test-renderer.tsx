import { render, RenderResult } from '@testing-library/react'
import { FelaProvider } from './fela-fela-provider'
import { getFelaRenderer } from './get-fela-renderer'

export function testRenderer(children: JSX.Element): RenderResult {
  return render(
    <FelaProvider renderer={getFelaRenderer()}>{children}</FelaProvider>,
  )
}
