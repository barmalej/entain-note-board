# fela

This library was generated with [Nx](https://nx.dev).

## CSS in JS implementation

This library provides a [Fels JS](https://fela.js.org/) setup. All [react](https://reactjs.org/) components in the project are using it to render styles.
