import { notesColors } from '..'

export type NoteColor = keyof typeof notesColors

export interface INote {
  id: string
  title: string
  text: string
  top?: number
  left?: number
  color: NoteColor
  uid: string
}

export type Notes = Record<string, INote>

export type NoteField = 'title' | 'text'

export interface IUser {
  id: string
  name: string
}
