export const globalDefaultStyle = /*css*/ `
* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;

  -ms-overflow-style: none;
  scrollbar-width: none;
}

*::-webkit-scrollbar {
  display: none;
}

button, input {
  all: unset;
}
`

export const notesColors = {
  persianPink: '#FF7EB9',
  frenchPink: '#FF65A3',
  electricBlue: '#7AFCFF',
  canary: '#FEFF9C',
  maximumYellow: '#FFF740',
}

export const colors = {
  ...notesColors,
  whiteSmoke: '#F2F2F2',
  white: '#FFFFFF',
  darkGray: '#AAAAAA',
  brightGray: '#EEEEEE',
  brandiesBlue: '#0069ED',
  absoluteZero: '#0053BA',
}
