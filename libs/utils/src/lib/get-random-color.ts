import { NoteColor, notesColors } from '..'

export const getRandomColor = () => {
  const colorNames = Object.keys(notesColors)
  const randomIndex = Math.round(Math.random() * (colorNames.length - 1))

  return colorNames[randomIndex] as NoteColor
}
