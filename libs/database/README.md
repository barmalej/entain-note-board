# database

This library was generated with [Nx](https://nx.dev).

## Firebase rules

The `rules.json` file is added to provide an overview of database setup. It is generic and is not meant for production use. There's also no setup for deploying rules. See [Manage Firebase rules](https://firebase.google.com/docs/rules/manage-deploy#deploy_your_updates) for more information.
