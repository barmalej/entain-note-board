import { NoteField } from '@entain/utils'
import { ref, set } from 'firebase/database'
import { getDatabase } from '..'

export const updateNote = (id: string, key: NoteField, value: string) =>
  set(ref(getDatabase(), `notes/${id}/${key}`), value)
