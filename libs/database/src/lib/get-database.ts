import { FirebaseOptions, initializeApp } from 'firebase/app'
import { Database, getDatabase as getDb } from 'firebase/database'

const firebaseConfig: FirebaseOptions = {
  apiKey: 'AIzaSyArxClVysDMG78RdUAfnZoZ_2kcdqRCXKQ',
  authDomain: 'enatin.firebaseapp.com',
  projectId: 'enatin',
  storageBucket: 'enatin.appspot.com',
  messagingSenderId: '994833931108',
  appId: '1:994833931108:web:b32741b3d55f7d87777871',
  databaseURL: ' https://enatin-default-rtdb.europe-west1.firebasedatabase.app',
}

export const getDatabase = () => {
  let db: Database
  try {
    db = getDb()
  } catch (error) {
    initializeApp(firebaseConfig)
    db = getDb()
  }

  return db
}
