import { getDatabase } from '@entain/db'
import { Box, Modal, NameModal, Spinner, WhiteBoard } from '@entain/ui'
import { getRandomColor, IUser, Notes, notesColors } from '@entain/utils'
import {
  getAuth,
  onAuthStateChanged,
  signInAnonymously,
  User,
} from 'firebase/auth'
import { onValue, ref } from 'firebase/database'
import { useEffect, useState } from 'react'
import * as styles from '../styles/index.style'

export function Index() {
  const [notes, setNotes] = useState<Notes | null | undefined>()
  const [fbUser, setFbUser] = useState<User | null>(null)
  const [user, setUser] = useState<IUser | null>(null)
  const [showNameModal, setShowNameModal] = useState(false)
  const [showSpinner, setShowSpinner] = useState(true)
  const db = getDatabase()

  useEffect(() => {
    if (fbUser?.uid) {
      onValue(ref(db, `users/${fbUser.uid}`), (snap) => {
        const usr = snap.val()

        setUser(usr)
        setShowNameModal(!usr?.id && !usr?.name)
      })
    }
  }, [fbUser])

  useEffect(() => {
    const auth = getAuth()
    signInAnonymously(auth)
    onAuthStateChanged(auth, setFbUser)

    onValue(ref(db, 'notes'), (snap) => {
      setNotes(snap.val())
    })
  }, [])

  useEffect(() => {
    if (user?.id && user?.name && typeof notes !== 'undefined') {
      setShowSpinner(false)
    }
  }, [user, notes])

  return (
    <Box style={styles.wrapper}>
      <Modal show={showSpinner} style={styles.modal}>
        <Spinner color={notesColors[getRandomColor()]} />
      </Modal>
      <NameModal show={showNameModal} />
      {!!user?.id && <WhiteBoard notes={notes} user={user} />}
    </Box>
  )
}

export default Index
