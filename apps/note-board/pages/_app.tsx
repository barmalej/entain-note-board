import { FelaProvider } from '@entain/fela'
import { IRenderer } from 'fela'
import { AppProps } from 'next/app'
import Head from 'next/head'

interface CustomAppProps extends AppProps {
  renderer: IRenderer
}

function CustomApp({ Component, pageProps, renderer }: CustomAppProps) {
  return (
    <>
      <Head>
        <title>Welcome to your Note Board!</title>
      </Head>
      <main>
        <FelaProvider renderer={renderer}>
          <Component {...pageProps} />
        </FelaProvider>
      </main>
    </>
  )
}

export default CustomApp
