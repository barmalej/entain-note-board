import { getFelaRenderer } from '@entain/fela'
import Document, {
  DocumentContext,
  Head,
  Html,
  Main,
  NextScript,
} from 'next/document'
import { ReactElement } from 'react'
import { renderToNodeList } from 'react-fela'

export default class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const renderer = getFelaRenderer()
    const originalRenderPage = ctx.renderPage

    ctx.renderPage = () =>
      originalRenderPage({
        // eslint-disable-next-line react/display-name
        enhanceApp: (App) => (props) => <App {...{ ...props, renderer }} />,
      })

    const initialProps = await Document.getInitialProps(ctx)
    const appStyles = initialProps.styles as ReactElement[]
    const styles = renderToNodeList(renderer)

    return {
      ...initialProps,
      styles: [...appStyles, ...styles],
    }
  }

  render() {
    return (
      <Html>
        <Head>
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link
            rel="preconnect"
            href="https://fonts.gstatic.com"
            crossOrigin="anonymous"
          />
          <link
            href="https://fonts.googleapis.com/css2?family=Indie+Flower:wght@400;700&display=swap"
            rel="stylesheet"
          />
          <link rel="shortcut icon" href="/favicon.png" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}
