import { IStyle } from 'fela'

export const wrapper: IStyle = { fontSize: '24px' }

export const modal: IStyle = {
  padding: '30px',
  borderRadius: '5px',
  background: 'transparent',
}
